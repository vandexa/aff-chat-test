module.exports = {
  env: {
    es6: true,
    node: true
  },
  extends: 'eslint:recommended',
  globals: {
    console: true,
    describe: true,
    it: true,
    expect: true,
    before: true,
    beforeEach: true,
    afterEach: true
  },
  parserOptions: {
    sourceType: 'module',
    ecmaFeatures: {
      modules: true,
      jsx: true
    }
  },
  rules: {
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    semi: ['error', 'always']
  }
};
