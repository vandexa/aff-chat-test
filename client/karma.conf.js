// Karma configuration

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['mocha', 'chai', 'sinon'],
    files: ['./src/**/*.spec.js'],
    exclude: [],
    preprocessors: {
      '**/*.spec.js': ['webpack']
    },

    reporters: ['progress'],
    webpack: require('./webpack//webpack.test.js'),
    webpackMiddleware: {
      stats: 'errors-only'
    },
    // web server port
    port: 9876,
    colors: true,
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'],
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,
    // how many browser should be started simultaneous
    concurrency: Infinity
  });
};
