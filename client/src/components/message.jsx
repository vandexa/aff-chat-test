import React from 'react';
// import './users.scss';

export default class MessageComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={'message' + (this.props.ownMessage ? ' own' : ' other')}>
        <span className="sender">
          {this.props.ownMessage ? '' : (this.props.message.from + ':')}
        </span>
        <span>{this.props.message.message}</span>
      </div>
    );
  }
}
