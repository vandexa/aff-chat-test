import React from 'react';
import Message from 'components/message';
import './chat.scss';

export default class ChatContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
      chat: [],
      users: props.users
    };
    this.submit = props.submit;
    this.user = props.user;
  }

  onSendMessage(event) {
    event.preventDefault();

    if (!this.state.input) {
      return;
    }

    this.submit(this.state.input, event);
    this.state.input = '';
    this.refs.inputField.value = '';
  }

  componentDidMount() {}

  componentDidUpdate() {
    // autoscroll
    const textOutput = this.refs.text_output;
    textOutput.scrollTop = textOutput.scrollHeight;
  }

  render() {
    return (
      <div className="chat-container">
        <ul className="user-list list-group">
          <div>
            {this.state.users.length} Users
          </div>
          {this.state.users.map((user, i) =>
            <li key={i}>
              {user}
            </li>
          )}
        </ul>
        <div className="chat">
          <div className="text-output" ref="text_output">
            {this.state.chat.map((message, i) =>
              <Message
                key={i}
                message={message}
                ownMessage={message.from === this.props.user}
              />
            )}
          </div>
          <form
            className="input-group text-input"
            onSubmit={e => this.onSendMessage(e)}
          >
            <input
              autoFocus
              className="form-control"
              type="text"
              ref="inputField"
              onChange={e => {
                this.state.input = e.target.value;
              }}
            />
            <span className="input-group-btn">
              <button className={"btn btn-primary" + (this.state.input ? '': ' disabled')} type="submit">
                Send
              </button>
            </span>
          </form>
        </div>
      </div>
    );
  }
}
