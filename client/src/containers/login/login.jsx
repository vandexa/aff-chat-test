import React from 'react';
import './login.scss';

export default class LoginContainer extends React.Component {
  constructor(props) {
    super(props);
    this.login = props.login;

    this.state = {
      name: '',
      validation: ''
    };
  }

  onSubmit(e) {
    e.preventDefault();

    if (!this.state.name) {
      this.setState({ validation: 'Please chose a name' });
      return;
    }

    const success = this.login(this.state.name);
    if (!success) {
      this.setState({ validation: 'Name has already been chosen' });
    }
  }

  render() {
    return (
      <form className="login">
        <h2>Affinitas Chat</h2>
        <input
          id="name-selection"
          className="form-control"
          placeholder="Pick a name"
          autoFocus
          type="text"
          onChange={e => (this.state.name = e.target.value)}
        />
        <button
          className="btn btn-primary"
          type="submit"
          onClick={e => this.onSubmit(e)}
        >
          Login
        </button>
        <div className="validation">
          {this.state.validation}
        </div>
      </form>
    );
  }
}
