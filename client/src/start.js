import io from 'socket.io-client';
import ReactDOM from 'react-dom';
import React from 'react';
import { LoginContainer, ChatContainer } from 'containers';
import 'main.scss';

const mainContainer = document.getElementById('main-container');
const socket = io.connect(__API__);

function renderLogin() {
  return ReactDOM.render(<LoginContainer login={connect} />, mainContainer);
}

function renderChat(name, users) {
  return ReactDOM.render(
    <ChatContainer user={name} users={users} submit={sendMesage} />,
    mainContainer
  );
}

function connect(name) {
  socket.emit('user:connect', { name }, (err, accepted) => {
    if (!accepted) {
      return false;
    }

    const chatContainer = renderChat(name, []);

    socket.emit('user:list', (err, list) => {
      const initialUserList = list.filter(user => user != name);
      chatContainer.setState({ users: initialUserList });
    });

    socket.on('user:connect', ({ name }) => {
      const newList = [name].concat(chatContainer.state.users);
      chatContainer.setState({ users: newList });
    });

    socket.on('user:disconnect', ({ name }) => {
      const newList = chatContainer.state.users.filter(user => user != name);
      chatContainer.setState({ users: newList });
    });

    socket.on('chat:message', message => {
      const chat = chatContainer.state.chat.slice(0);
      chat.push(message);

      chatContainer.setState({ chat: chat });
    });
  });
}

function sendMesage(msg) {
  socket.emit('chat:message', msg);
}

renderLogin();
