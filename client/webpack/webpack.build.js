const path = require('path'),
  webpack = require('webpack'),
  ExtractTextPlugin = require('extract-text-webpack-plugin'),
  HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: {
    app: './src/start.js',
    vendor: ['lodash', 'react', 'react-dom', 'socket.io-client']
  },
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'app.js',
    pathinfo: true
  },
  resolve: {
    modules: ['node_modules', './src/'],
    extensions: ['.js', '.jsx'],
    alias: {
      styles: path.resolve(__dirname, '../src/styles')
    }
  },
  module: {
    loaders: [
      {
        test: /\.jsx?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loaders: ['react-hot-loader', 'babel-loader'],
        exclude: /(\/node_modules\/|\.spec\.js$)/
      },
      {
        test: /\.(eot|otf|ttf|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file-loader?name=assets/fonts/[name].[ext]'
      },
      {
        test: /\.scss(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: ExtractTextPlugin.extract('css-loader!sass-loader')
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      __API__: JSON.stringify('http://localhost:8080')
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'vendor.bundle.js'
    }),
    new webpack.LoaderOptionsPlugin({ minimize: true, debug: false }),
    new webpack.optimize.UglifyJsPlugin(),
    new ExtractTextPlugin('[name].css'),
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ]
};
