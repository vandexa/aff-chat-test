#Affinitas Chat Test

##Setup
In both client and server folders:
```bash
npm install
```

##Test the server
```bash
npm test
```

##Run
In both client and server folders:
```bash
npm start
```

##Client Build
```bash
npm run build
```
This will create a dist folder that can be hosted.
