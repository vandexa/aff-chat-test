const
get = require('lodash/get'),
userModel = require('../models/user.model');

module.exports = function chatHandler(io, socket) {
  socket.on('chat:message', message => {
    userModel.findBySocketId(socket.id, (err, sender) => {
      io.emit('chat:message', { message, from: get(sender, 'name', 'unknown') });
    });
  });
};
