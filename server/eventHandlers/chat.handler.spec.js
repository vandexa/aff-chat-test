const io = require('socket.io-client'),
  chai = require('chai'),
  userModel = require('../models/user.model'),
  { expect } = chai;
const { uri } = require('../start');

describe('Chat Handler', () => {
  const serverUri = `http://${uri}`,
    serverOptions = {
      'reconnection delay': 0,
      'reopen delay': 0,
      'force new connection': true
    };
  let client1, client2;

  beforeEach(function(done) {
    userModel.clear();
    client1 = io.connect(serverUri, serverOptions);
    client1.on('connect', function() {
      client1.emit('user:connect', { name: 'fred' });
      client2 = io.connect(serverUri, serverOptions);
      client2.on('connect', function() {
        client2.emit('user:connect', { name: 'bert' });
        done();
      });
    });
  });

  afterEach(function(done) {
    if (client1.connected) {
      client1.disconnect();
    }
    if (client2.connected) {
      client2.disconnect();
    }
    done();
  });

  it('should allow a user to send a message to other connected users', done => {
    client1.on('chat:message', chat => {
      expect(chat).to.have.property('message', 'hello');
      expect(chat).to.have.property('from', 'bert');
      done();
    });
    client2.emit('chat:message', 'hello');
  });

  it('should allow a user to receive the message they sent', done => {
    client1.on('chat:message', chat => {
      expect(chat).to.have.property('message', 'is it me?');
      expect(chat).to.have.property('from', 'fred');
      done();
    });
    client1.emit('chat:message', 'is it me?');
  });
});
