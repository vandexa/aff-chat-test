const userModel = require('../models/user.model');

module.exports = function userHandler(io, socket) {
  socket.on('user:connect', ({ name }, cb) => {
    userModel.add({ name, socketId: socket.id }, (err, addedUser) => {
      if (!err && addedUser) {
        socket.broadcast.emit('user:connect', { name });
        return cb && cb(null, true);
      }

      cb && cb(err);
    });
  });

  socket.on('user:list', cb => {
    userModel.list(cb);
  });

  socket.on('disconnect', () => {
    userModel.removeBySocketId(socket.id, (err, user) => {
      if (user && user.name) {
        io.emit('user:disconnect', { name: user.name });
      }
    });
  });
};
