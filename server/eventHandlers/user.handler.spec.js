const io = require('socket.io-client'),
  aParallel = require('async/parallel'),
  aSeries = require('async/series'),
  forEach = require('lodash/forEach'),
  chai = require('chai'),
  userModel = require('../models/user.model'),
  { expect } = chai;
const { uri } = require('../start');

describe('User Handler', () => {
  const serverUri = `http://${uri}`,
    serverOptions = {
      'reconnection delay': 0,
      'reopen delay': 0,
      'force new connection': true
    };
  let client1;

  beforeEach(function(done) {
    userModel.clear();
    client1 = io.connect(serverUri, serverOptions);
    client1.on('connect', function() {
      done();
    });
  });

  afterEach(function(done) {
    if (client1.connected) {
      client1.disconnect();
    }
    done();
  });

  it('should allow a user to connect', done => {
    client1.emit('user:connect', { name: 'fred' }, (err, added) => {
      expect(err).to.be.null;
      expect(added).to.be.true;
      done();
    });
  });

  it('should allow multiple connected users', done => {
    const client2 = io.connect(serverUri, serverOptions),
      client3 = io.connect(serverUri, serverOptions);

    aParallel(
      [
        cb => client1.emit('user:connect', { name: 'dirk' }, cb),
        cb => client2.emit('user:connect', { name: 'jef' }, cb),
        cb => client3.emit('user:connect', { name: 'luk' }, cb)
      ],
      (err, adds) => {
        expect(err).to.be.null;
        forEach(adds, add => expect(add).to.not.be.null);
        client2.disconnect();
        client3.disconnect();
        done();
      }
    );
  });

  it('should signal other clients a user connected', done => {
    const client2 = io.connect(serverUri, serverOptions);

    client1.on('user:connect', ({ name }) => {
      expect(name).to.equal('bram');
      client2.disconnect();
      done();
    });

    client2.emit('user:connect', { name: 'bram' });
  });

  it('should not signal users if a user tries to connect with an existing name', done => {
    const client2 = io.connect(serverUri, serverOptions);

    aSeries(
      [
        cb => client1.emit('user:connect', { name: 'dirk' }, cb),
        cb => client2.emit('user:connect', { name: 'jef' }, cb)
      ],
      (err, adds) => {
        expect(err).to.be.null;

        let usersConnected = 0;
        client1.on('user:connect', () => {
          usersConnected += 1;
        });
        client2.on('user:connect', () => {
          usersConnected += 1;
        });

        setTimeout(() => {
          expect(usersConnected).to.equal(0);
          client2.disconnect();
          done();
        }, 25);
      }
    );
  });

  it('should signal other clients a user disconnected', done => {
    const client2 = io.connect(serverUri, serverOptions);
    client2.emit('user:connect', { name: 'tom' }, () => {
      client2.disconnect();
    });

    client1.on('user:disconnect', ({ name }) => {
      expect(name).to.equal('tom');
      done();
    });
  });

  it('should provide a list of users', done => {
    const client2 = io.connect(serverUri, serverOptions),
      client3 = io.connect(serverUri, serverOptions);

    client1.emit('user:connect', { name: 'bert' });
    client2.emit('user:connect', { name: 'bart' });
    client3.emit('user:connect', { name: 'bort' });

    client1.emit('user:list', (err, list) => {
      expect(err).to.be.null;
      expect(list).to.be.an('array').that.includes('bert', 'bart', 'bort');
      done();
    });
  });
});
