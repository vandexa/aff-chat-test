const map = require('lodash/map'),
  remove = require('lodash/remove'),
  first = require('lodash/first'),
  find = require('lodash/find');

const users = [
  /*{ name, socketId }*/
];

module.exports = {
  add(user, cb = function() {}) {
    if (!user) {
      return cb(new Error('invalid operation'));
    }

    const { name, socketId } = user;
    if (!name || !socketId) {
      return cb(new Error('invalid operation'));
    }

    const exist = find(users, { name });
    if (exist) {
      return cb(new Error('userExist'));
    }

    users.push(user);
    cb(null, user);
  },
  findByName(name, cb = function() {}) {
    const user = find(users, { name });

    cb(null, user);
  },
  findBySocketId(socketId, cb = function() {}) {
    const user = find(users, { socketId });

    cb(null, user);
  },
  removeBySocketId(socketId, cb = function() {}) {
    const removedUser = first(remove(users, { socketId }));

    cb(null, removedUser);
  },
  list(cb = function() {}) {
    cb(null, map(users, 'name'));
  },
  clear(cb = function() {}) {
    users.length = 0;
    cb(null);
  }
};
