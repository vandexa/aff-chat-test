const chai = require('chai'),
  aParallel = require('async/parallel'),
  aSeries = require('async/series'),
  userModel = require('./user.model'),
  { expect } = chai;
require('../start');

beforeEach(done => {
  userModel.clear(() => done());
});

describe('User Model', () => {
  it('should allow users to be added', done => {
    userModel.add({ name: 'jef', socketId: 'bla' }, (err, addedUser) => {
      expect(err).to.be.null;
      expect(addedUser).to.have.property('name', 'jef');
      expect(addedUser).to.have.property('socketId', 'bla');
      done();
    });
  });

  it('should only add a user with a name and a socketId', done => {
    userModel.add({ name: 'jef' }, (err, addedUser) => {
      expect(err).to.be.an('error');
      expect(addedUser).to.be.undefined;
      userModel.add({ socketId: 'bla' }, (err, addedUser) => {
        expect(err).to.be.an('error');
        expect(addedUser).to.be.undefined;
        userModel.add(null, (err, addedUser) => {
          expect(err).to.be.an('error');
          expect(addedUser).to.be.undefined;
          done();
        });
      });
    });
  });

  it('should have a list of all added users', done => {
    aParallel(
      [
        cb => userModel.add({ name: 'dirk', socketId: 'bla1' }, cb),
        cb => userModel.add({ name: 'bert', socketId: 'bla2' }, cb),
        cb => userModel.add({ name: 'jan', socketId: 'bla3' }, cb)
      ],
      (err, usersAdded) => {
        userModel.list((err, list) => {
          expect(err).to.be.null;
          expect(list).to.have.property('length', 3);
          done();
        });
      }
    );
  });

  it('should allow to find a user by name', done => {
    userModel.add({ name: 'dirk', socketId: 'dirkid' }, (err, user) => {
      userModel.findByName('dirk', (err, user) => {
        expect(user).to.not.be.undefined;
        expect(user).to.have.property('socketId', 'dirkid');
        done();
      });
    });
  });

  it('should allow to find a user by socketId', done => {
    userModel.add({ name: 'bert', socketId: 'bertid' }, (err, user) => {
      userModel.findBySocketId('bertid', (err, user) => {
        expect(user).to.not.be.undefined;
        expect(user).to.have.property('name', 'bert');
        done();
      });
    });
  });

  it('should allow users to be removed by socket id', done => {
    aSeries(
      [
        cb => userModel.add({ name: 'dirk', socketId: 'id' }, cb),
        cb => userModel.removeBySocketId('id', cb),
        cb => userModel.list(cb)
      ],
      (err, values) => {
        expect(err).to.be.null;
        expect(values[2]).to.have.property('length', 0);
        done();
      }
    );
  });

  it('should not allow 2 users with the same name', done => {
    aSeries(
      [
        cb => userModel.add({ name: 'dirk', socketId: 'bla1' }, cb),
        cb => userModel.add({ name: 'dirk', socketId: 'bla2' }, cb)
      ],
      (err, usersAdded) => {
        expect(err).to.be.an('error');
        userModel.list((err, list) => {
          expect(err).to.be.null;
          expect(list).to.have.property('length', 1);
          done();
        });
      }
    );
  });
});
