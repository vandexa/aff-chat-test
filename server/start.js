const
restify        = require('restify'),
corsMiddleware = require('restify-cors-middleware'),
socketIO       = require('socket.io'),
userHandler    = require('./eventHandlers/user.handler'),
chatHandler    = require('./eventHandlers/chat.handler');

function startServer(port = 8080) {
  const uri    = 'localhost:' + port,
        server = restify.createServer({
            name: 'aff-chat-test-api',
            version: '1.0.0'
        });

  server.use(corsMiddleware({ origins: ['*'] }).actual);

  server.listen(port);
  const io = socketIO.listen(server.server);

  console.log('Server address:', server.address());

  return { io, uri, server };
}

const { uri, server, io } = startServer();

server.on('uncaughtException', (req, res, route, err) => {
  console.error('UNCAUGHT EXCEPTION:', err);
});

io.sockets.on('connection', function(client) {
  userHandler(io, client);
  chatHandler(io, client);
});

// for testing
module.exports = { uri };
